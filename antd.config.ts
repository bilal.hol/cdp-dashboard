import { ThemeConfig } from "antd";

const antdConfig: ThemeConfig = {
  components: {
    Menu: {
      itemSelectedBg: "#F5F5FF",
      itemSelectedColor: "#003366",
      itemBorderRadius: 3,
    },
  },
};

export default antdConfig;
