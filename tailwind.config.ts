import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        black: "#333333",
        white: "#FFFFFF",
        "muted-ds2": "#FAFAFA",
        muted: "#777777",
        neutral: {
          50: "#F5F7F9",
          75: "#CFD9E2",
          100: "#A8B8C7",
          200: "#8398AB",
          300: "#607A93",
          400: "#2B4156",
          500: "#162533",
          white: "#FFFFFF",
        },
        primary: {
          100: "#E0ECF9",
          200: "#89BFF4",
          300: "#2877C5",
          400: "#0052A3",
          500: "#05213E",
          600: "#002953",
          main: "#003366",
          surface: "#F5F5FF",
        },
        secondary: {
          100: "#FFF3C5",
          200: "#FFE168",
          500: "#8D7201",
          main: "#F8CA0F",
        },
        warning: {
          50: "#FFFAE6",
          100: "#FFF9E3",
          300: "#DAA520",
          500: "#6F4E37",
        },
        success: {
          100: "#EEFDF6",
          300: "#00875A",
          500: "#006040",
          main: "#29CA40",
        },
        danger: {
          100: "#FFEFF0",
          300: "#DC381F",
          500: "#800000",
        },
        info: {
          100: "#EBF6FC",
          150: "#2292D3",
          200: "#39A3DF",
          300: "#0177CC",
          500: "#00458A",
        },
        background: {
          light: "#F2F2F2",
          line: "#E8E8E8",
          grey: "#CFD9E280",
          border: "#C7C7C7",
          screen: "#F7F7F7",
        },
      },
      fontFamily: {
        sans: ["Nunito Sans", "Helvetica Neue", "sans-serif"],
        inter: ["Inter", "Helvetica Neue", "sans-serif"],
        montserrat: ["Montserrat", "Helvetica Neue", "sans-serif"],
      },
    },
  },
  plugins: [],
};
export default config;
