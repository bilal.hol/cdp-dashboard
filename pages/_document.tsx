import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <Head>
          <meta name="title" content="Customer Data Platform | Hukumonline" />
          <link rel="shortcut icon" href={"/img/favicon.ico"} />
        </Head>
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
