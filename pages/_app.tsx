import "@/styles/globals.css";
import { Breadcrumb, ConfigProvider, Layout } from "antd";
import type { AppProps } from "next/app";

import Head from "next/head";
import HeaderNav from "./component/layout/Header";
import SideNav from "./component/layout/Sidebar";
import antdConfig from "@/antd.config";
const { Content } = Layout;

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Customer Data Platform Dashboard</title>
      </Head>
      <main className="app">
        <ConfigProvider theme={antdConfig}>
          <Layout className="h-screen">
            <HeaderNav />
            <Layout>
              <SideNav />
              <Layout className="bg-primary-surface pt-4 px-6">
                <Breadcrumb className="mt-4">
                  <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                  <Breadcrumb.Item>Customer Management</Breadcrumb.Item>
                </Breadcrumb>
                <Content className="py-4">
                  <Component {...pageProps} />
                </Content>
              </Layout>
            </Layout>
          </Layout>
        </ConfigProvider>
      </main>
    </>
  );
}

export default CustomApp;
