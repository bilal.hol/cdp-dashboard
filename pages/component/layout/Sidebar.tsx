import { Menu } from "antd";
import type { MenuProps } from "antd";
import { Layout } from "antd";
const { Sider } = Layout;

const SideNav = () => {
  const menuList = [
    "Dashboard",
    "Customer Management",
    "User Management",
    "Report",
  ];

  const menus: MenuProps["items"] = [
    {
      key: "dashboard-menu",
      label: "Dashboard",
      disabled: true,
    },
    {
      key: "customer-management-menu",
      label: "Customer Management",
      disabled: false,
    },
    {
      key: "user-management-menu",
      label: "User Management",
      disabled: true,
    },
    {
      key: "report-menu",
      label: "Report",
      disabled: true,
    },
  ];

  return (
    <Sider className="w-32 shadow-sm min-w-40">
      <Menu
        mode="inline"
        items={menus}
        className="bg-white h-full"
        selectedKeys={["customer-management-menu"]}
      />
    </Sider>
  );
};

export default SideNav;
