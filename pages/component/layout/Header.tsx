import Image from "next/image";
import { Layout, Avatar, Space, Button } from "antd";
const { Header } = Layout;
import { DownOutlined, UserOutlined } from "@ant-design/icons";
import { useState } from "react";

const HeaderNav = () => {
  const [onClickProfile, setOnClickProfile] = useState<boolean>(false);

  return (
    <Header className="flex justify-between items-center py-2 px-10 bg-white shadow-2xl">
      <div>
        <Image
          src={"/img/hukumonline-logo.png"}
          alt="hukumonline-logo"
          height={90}
          width={90}
        />
      </div>
      <div tw="bg-neutral-100 flex gap-1">
        <Button
          type="text"
          block
          className="flex gap-2 items-center py-5 px-1"
          onClick={() => {
            setOnClickProfile(!onClickProfile);
          }}
        >
          <Space wrap size={13}>
            <Avatar size="default" icon={<UserOutlined />} />
          </Space>
          <DownOutlined
            className="text-xs font-bold"
            rotate={onClickProfile ? 180 : 0}
          />
        </Button>
      </div>
    </Header>
  );
};

export default HeaderNav;
